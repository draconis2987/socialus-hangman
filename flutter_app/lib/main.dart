import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home :Scaffold(
        appBar: AppBar(title: Text('SocialUs Hangman')),
        body: Column(children: <Widget>[
          Image.asset('assets/images/gallows_mdpi.png'
          ),
          Text('Hangman in a Widget')
        ],),
      )
    );
  }
}
